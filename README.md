# Site selector

A simple, responsive, beautiful site selector
-------------------------------------------------

### How to use it?
1. Download **index.html**
2. Search for every "[#]" mentions in the file and replace them with the needed informations
3. Place **index.html** where you want to put the site selector on your website
   (eg. in a sub-folder "/site-selector" to put the site selector in "[http://yourwebsite.tld/site-selector](http://yourwebsite.tld/site-selector)")
4. Go to your website where the file is located, tadaa!